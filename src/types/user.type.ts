/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

import { Maybe } from './utils';

export interface TUser {
  avatar: Maybe<string>;
  email: Maybe<string>;
  id: Maybe<number>;
  login: Maybe<string>;
  user_token: Maybe<string>;
  name: string;
  phone: string;
}
