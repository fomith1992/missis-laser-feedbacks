/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { Routes } from '../shared/enums/routes';
import { MainPage } from './main-page';

export const AppRouter = () => {
  useEffect(() => {
    const onResize = () => {
      let innerHeight = window.innerHeight;
      document.documentElement.style.setProperty('--app-height', `${innerHeight}px`);
    };

    onResize();

    window.addEventListener('resize', onResize);

    return () => {
      window.removeEventListener('resize', onResize);
    };
  }, []);

  return <MainPage />;
};
