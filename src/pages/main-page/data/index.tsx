/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

export const data = [
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/001.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/002.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/003.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/004.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/005.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/006.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/007.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/008.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/009.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/010.png',
  },
  {
    type: 'image',
    url: 'http://media.missis-laser.ru/missis-laser/011.png',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/001.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/002.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/003.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/004.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/005.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/006.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/007.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/008.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/009.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/010.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/011.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/012.MOV',
  },
  {
    type: 'video',
    url: 'http://media.missis-laser.ru/missis-laser/013.MOV',
  },
];
