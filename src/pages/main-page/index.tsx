/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

import { Box, Text } from '@chakra-ui/react';
import styled from '@emotion/styled';
import { Button, Carousel, Spin } from 'antd';
import { CarouselRef } from 'antd/lib/carousel';
import React, { useLayoutEffect, useRef, useState } from 'react';
import { data } from './data';
import { BigPlayButton, ControlBar, Player } from 'video-react';
import { useInterval } from 'beautiful-react-hooks';

const slideWidth = 310;

function shuffle(
  array: {
    type: string;
    url: any;
  }[]
) {
  return array.sort(() => Math.random() - 0.5);
}

export const MainPage = (): React.ReactElement => {
  const carousel = useRef<CarouselRef>(null);
  const [cardsCount, setCardsCount] = useState(6);
  const [isVisible, setisVisible] = useState(false);

  useLayoutEffect(() => {
    const handleImageCountChange = () => {
      if (window.innerWidth >= 1920 + slideWidth) return 8;
      if (window.innerWidth >= 1920) return 7;
      if (window.innerWidth >= 1920 - slideWidth) return 6;
      if (window.innerWidth >= 1920 - slideWidth * 2) return 5;
      if (window.innerWidth >= 1920 - slideWidth * 3) return 4;
      if (window.innerWidth >= 1920 - slideWidth * 4) return 3;
      return 1;
    };

    function updateSize() {
      setCardsCount(handleImageCountChange());
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  useInterval(() => {
    if (!isVisible) {
      setisVisible(true);
    }
  }, 10000);

  const sliderData = [...data];

  console.log('xxx');

  return (
    <div style={{ position: 'relative', overflow: 'hidden' }}>
      <div style={{ position: 'relative', opacity: isVisible ? 1 : 0 }}>
        <Text
          style={{
            fontSize: 40,
            fontFamily: 'sans-serif',
            textAlign: 'center',
            marginTop: '16px',
            marginBottom: '16px',
            fontWeight: 800,
          }}
        >
          Что говорят наши клиенты
        </Text>
        <Carousel autoplay centerMode slidesToShow={cardsCount} dots ref={carousel}>
          {shuffle(sliderData).map((item, idx) =>
            item.type === 'image' ? (
              <OverflowContainer key={idx}>
                <Container>
                  <img
                    style={{ objectFit: 'cover', width: '100%', height: '100%' }}
                    src={item.url}
                    alt={'Тут картинка с отзывом'}
                  />
                </Container>
              </OverflowContainer>
            ) : (
              <OverflowContainer key={idx}>
                <Container>
                  <div style={{ height: 513 }}>
                    <Player height={513} width={289} src={item.url} fluid>
                      <BigPlayButton position='center' />
                      <ControlBar disableCompletely />
                    </Player>
                  </div>
                </Container>
              </OverflowContainer>
            )
          )}
        </Carousel>
        <div
          style={{
            width: '80%',
            margin: '0 auto',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Button
            onClick={() => {
              carousel.current?.prev();
            }}
            type='primary'
            shape='round'
            style={{
              backgroundColor: '#fff',
              color: '#fff',
              borderColor: '#fff',
              marginTop: '12px',
              fontSize: '12px',
              height: '60px',
            }}
          >
            <img className='arrow-button' src={require('./left.png').default} alt='specialistImage' />
          </Button>
          <Button
            onClick={() => {
              carousel.current?.next();
            }}
            type='primary'
            shape='round'
            style={{
              backgroundColor: '#fff',
              color: '#fff',
              borderColor: '#fff',
              marginTop: '12px',
              fontSize: '12px',
              height: '60px',
            }}
          >
            <img className='arrow-button' src={require('./right.png').default} alt='specialistImage' />
          </Button>
        </div>
      </div>
      {!isVisible && (
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Spin size='large' />
        </div>
      )}
    </div>
  );
};

const OverflowContainer = styled(Box)`
  margin-top: 12px;
  margin-bottom: 32px;
  display: block;
  position: relative;
  height: 513px;
  width: ${slideWidth}px;
  overflow: hidden;
`;

const Container = styled(Box)`
  display: block;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  margin-left: 6px;
  margin-right: 6px;
  background-color: #000;
  overflow: hidden;
  border-radius: 20px;
`;
